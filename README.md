# Printi Commerce Challenge
Este é um desafio da equipe de Tech da Printi para Desenvolvedores Front-End Sênior

## O que terá neste desafio?
Você criará um pequeno ecommerce com apenas duas páginas: A home e a página do produto. O seu conteúdo dinâmico estará disponível em uma API dentro do próprio projeto. Este ecommerce precisa atender os requisitos mínimos de tecnologias, métricas e informações.

### Configurando o desafio
Clone este repositório em sua máquina de desenvolvimento e adicione as seguintes tecnologias:

* eslint e prettier
* testing-library e/ou jest para os testes
* axios para requisições
* styled components para o CSS-in-JS
* Redux Saga
* Babel


### Criação do Header
No cabeçalho do nosso ecommerce deve possuir:

* Logo da empresa (pasta public)
* Campo de busca de produtos usando a rota `/products`, mostrando apenas 3 produtos com imagem, nome e preço (Similar com a imagem abaixo)
* Carrinho com a quantidade de produtos adicionados
* Menu com todas as categórias e produtos usando a rota `/categories` (Similar com a imagem abaixo)
* Teste de hoooks 

![picture](public/header.png)
![picture](public/busca.png)
![picture](public/menu.png)

### Criação do Footer
No rodapé do nosso ecommerce deve possuir:

* Lista de páginas institucionais usando a rota `/footer-links`
* Formas de pagamento com várias bandeiras de cartão de crédito
* Teste de integração com a rota `/footer-links`

### Criação da home
A página inicial deve ter:

* os 15 produtos mais vendidos da empresa na rota `/products`
* Criar uma função que ordenará a lista destes 15 produtos pela chave "sort" e criar um teste unitário para esta função
* O card do produto deve conter Uma imagem, nome, preço, quantidade mínima e um botão para ver o produto e outro botão para adicionar o produto diretamente ao carrinho. (Similar com a imagem abaixo)
* O botão de `adicionar ao carrinho` deve usar a rota `/cart` para a inclusão do produto e atualizar a quantidade de produtos no carrinho do header. Obrigatório o uso do redux-saga para essa ação
* Test de snpashot para o card do produto

![picture](public/card-produto.png)

### Página do produto

A página do produto deve conter:
* Criar um custom hooks para fazer a requisição do produto chamada `useFetchProduct` usando a rota `products/[product-name]`
* Criar um teste de hook para o `useFetchProduct`
* Um carrossel de imagens do produto
* Nome do produto
* Subtítulo
* Preço
* Quantidade mínima
* Botão de adição do produto ao carrinho (Mesma lógica da home)
* Detalhes do produto

### Requisitos finais

* Usar rem ao invés de pixels(px)
* Não deve possuir nenhum erro apontado pelo eslint
* Todas as páginas devem ser responsivas
* Funcionar corretamente em todos os principais navegadores
* O projeto deve buildar corretamente
* Lighthouse: As duas páginas devem alcançar 100 pontos em Acessibilidade, Melhores práticas e SEO.

![picture](public/lighthouse.png) 

### Pontos extras

* Criar tests para toda a aplicação
* Usar o rich serach results na página de produto: https://developers.google.com/search/docs/data-types/product
* Usar o `getServerSideProps` server-side redering nas duas páginas: https://nextjs.org/docs/basic-features/data-fetching#getserversideprops-server-side-rendering

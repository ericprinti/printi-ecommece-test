import React, { memo, useEffect, useState } from 'react';
import axios from 'axios';
import Head from 'next/head';
import Image from 'next/image';
import styles from '../styles/Home.module.css'

const Home = () => (
  <div className={styles.container}>
    <Head>
      <title>Printi Commerce</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>

    <main className={styles.main}>
      <img className={styles.logo} src="/logo.svg" alt="Printi Logo" className={styles.logo} />
      <h1 className={styles.title}>
        Printi Ecommerce
      </h1>

      <h2 className={styles.description}>
        Welcome to Printi Ecommerce Challenge! Good luck!
      </h2>
    </main>
  </div>
);

export default memo(Home);
